module gitlab.com/gitlab-org/release-cli

go 1.16

require (
	github.com/cyphar/filepath-securejoin v0.2.3
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1
	github.com/hashicorp/go-version v1.4.0 // indirect
	github.com/jstemmer/go-junit-report v1.0.0
	github.com/mitchellh/gox v1.0.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.1
	github.com/urfave/cli/v2 v2.4.0
	golang.org/x/sys v0.0.0-20220406163625-3f8b81556e12 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
